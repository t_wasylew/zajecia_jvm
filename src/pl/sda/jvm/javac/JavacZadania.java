package pl.sda.jvm.javac;

import java.util.Scanner;

public class JavacZadania {

    public static void main(String[] args) {

        while(true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print(">>>>");
            String n = scanner.nextLine();
            if (n.trim().toLowerCase().equals("exit")) {
                System.out.println("Quitting");
                break;
            }
            printHello(n);
        }
    }

    private static void printHello(String n) {
        int parseInt =1;
        try {
            parseInt = Integer.parseInt(n);
        } catch (NumberFormatException nfe) {
            System.out.println("Wrong input");
        }
        for (int i = 0; i < parseInt; i++) {
            System.out.println("Hello World!");
        }
        System.out.println();
    }
}
